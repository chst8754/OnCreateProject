﻿using UnityEngine;
using UnityEngine.Events;

public abstract class Card : ScriptableObject
{
    [Header("Card Properties")]
    /// <summary>
    /// Name of the card
    /// </summary>
    public new string name;
    /// <summary>
    /// Has the card been unlocked?
    /// </summary>
    public bool unlocked;
    /// <summary>
    /// Cost of the card
    /// </summary>
    [Range(0, 100)]
    public int baseCost;
    /// <summary>
    /// Description of the card
    /// </summary>
    [TextArea]
    public string description;
    /// <summary>
    /// Image of the card
    /// </summary>
    public Sprite defaultSprite;
    public Sprite cardSprite;

    public Vector2 colliderOffset;
    public Vector2 colliderSize;
    public Vector3 spriteScale;
}
