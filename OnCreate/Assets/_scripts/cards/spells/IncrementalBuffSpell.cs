﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class IncrementalBuffSpell : BuffSpell
{
    public abstract int Buff(int property);
    public abstract float Buff(float property);
    // public abstract Unit.MovementSpeed Buff(Unit.MovementSpeed property);
}
