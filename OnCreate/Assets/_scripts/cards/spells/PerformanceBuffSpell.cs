﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "OnCreate/Cards/Spell/New Performance Buff")]
public class PerformanceBuffSpell : IncrementalBuffSpell
{
    // Healthpoints, AttackDamage, AttackRange, AttackSpeed, Movementspeed, Spawn, Death, Attack
    [Range(1f, 10f)]
    public float amount;
    public override void ApplySpell(SpellTarget target)
    {
        Action initial = null;
        int len = affectedTypes.Length;
        for (int i = 0; i < len; i++)
        {
            UnitPropertyType prop = affectedTypes[i];
            switch (prop)
            {
                case UnitPropertyType.AttackDamage:
                    initial = () => target.unit.attackPoints = Buff(target.unit.attackPoints);
                    break;
                case UnitPropertyType.AttackRange:
                    initial = () => target.unit.attackRange = Buff(target.unit.attackRange);
                    break;
                case UnitPropertyType.AttackSpeed:
                    initial = () => target.unit.attackSpeed = Buff(target.unit.attackSpeed);
                    break;
                case UnitPropertyType.Movementspeed:
                    break;
                case UnitPropertyType.Healthpoints:
                    initial = () => target.health.healthPoints = Buff(target.health.healthPoints);
                    break;
                case UnitPropertyType.Death:
                    break;
                case UnitPropertyType.Attack:
                    break;
                default:
                    Debug.Log("Something went very wrong with " + name);
                    break;
            }
            target.StartCoroutine(target.OverTime(duration, initial));
        }
    }
    public override int Buff(int property)
    {
        return Mathf.CeilToInt(property * amount);
    }

    public override float Buff(float property)
    {
        return property * amount;
    }
}
