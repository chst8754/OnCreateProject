﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "OnCreate/Cards/Spell/New Explode Buff")]
public class ExplodeBuffSpell : BuffSpell
{
    public int explosionDamage;
    public Target[] explosionTargets;
    public override void ApplySpell(SpellTarget target)
    {
        // Plague - Adjacent units take Damage after they die
        HealthbarController.OnDeath dOnDeath = null;
        dOnDeath = () =>
        {
            Explode(target);
            target.health.UnsubscribeDeathFunction(dOnDeath);
        };
        target.health.SubscribeDeathFunction(dOnDeath);
    }
    private void Explode(SpellTarget target)
    {
        int len = explosionTargets.Length;
        LayerMask friendlyLayer = target.unit.commander.allyMask;
        LayerMask enemyLayer = target.unit.commander.enemyMask;
        for (int i = 0; i < len; i++)
        {
            Collider2D[] hits = null;
            switch (explosionTargets[i])
            {
                case Target.Friendly:
                    hits = Physics2D.OverlapCircleAll(target.transform.position, baseRadius, friendlyLayer);
                    break;
                case Target.Enemy:
                    hits = Physics2D.OverlapCircleAll(target.transform.position, baseRadius, enemyLayer);
                    break;
                default:
                    Debug.LogWarning("Forgot to add a layer in SpellController DirtyAnd");
                    break;
            }
            if (hits != null)
            {
                int _len = hits.Length;
                for(int j = 0; j < _len; j++)
                {
                    SpellTarget st = hits[j].transform.parent.GetComponent<SpellTarget>();
                    if (st != null)
                    {
                        st.health.TakeDmg(explosionDamage);
                    }
                }
            }
        }
    }
}
