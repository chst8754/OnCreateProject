﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName ="OnCreate/Building")]
public class Building : ScriptableObject {

    [Header("Building Properties")]
    public new string name;
    [TextArea]
    public string description;
    [Range(0, 5000)]
    public int baseHealthPoints;
    public Vector2 colliderOffset;
    public Vector2 colliderSize;
    public Vector3 spriteScale;

    public GameObject prefab;
    
}
