﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIResourceController : ResourceController
{
    void Start()
    {
        this.currentResourceAmount = resource.baseMinimum;
        this.currentMaximum = resource.baseMaximum;
        this.currentRegenerationRate = 1f / resource.regenerationPerSecond;
        StartCoroutine(RegenerationCycle());
    }

    public override IEnumerator RegenerationCycle()
    {
        while (gameObject.activeInHierarchy)
        {
            IncreaseResource(currentRegenerationAmount);
            yield return new WaitForSeconds(currentRegenerationRate);
        }
    }

    public override void IncreaseResource(int amount)
    {
        if ((currentResourceAmount + amount) < currentMaximum)
        {
            currentResourceAmount += amount;
        }
        else
        {
            currentResourceAmount = currentMaximum;
            if (debug) Debug.LogWarning("Resource " + resource.name + " is full");
        }
    }

    public override bool PayCost(Card card)
    {
        if (card.baseCost < currentResourceAmount)
        {
            currentResourceAmount -= card.baseCost;
            return true;
        }
        else
        {
            return false;
        }
    }
}