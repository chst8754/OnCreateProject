﻿using System.Linq;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    public bool debug;
    public BuildingController allyBase;
    public LayerMask allyMask;
    public BuildingController enemyBase;
    public LayerMask enemyMask;
    public Pool<UnitController>[] units;
    public Unit[] unitLineup;
    public Spell[] spellLineup;
    public Vector3 spawnPosition;
    public Vector3 SpawnPosition
    {
        get
        {
            return allyBase.transform.position + spawnPosition;
        }
    }
    public Vector3 spellPosition;
    public Vector3 SpellPosition
    {
        get
        {
            return allyBase.transform.position + spellPosition;
        }
    }
    public Pool<SpellController>[] spells;
    // Resource Management
    private ResourceController unitResourceController;
    private ResourceController spellResourceController;
    public Card[] deck;

    void Start()
    {
        // Acquire the deck
        deck = PersistentDataManager.Instance.player_deck;
        // Align the allyBase
        float x = Camera.main.ScreenToWorldPoint(InterfaceManager.Instance.ScreenSize * 0.9f).x;
        allyBase.transform.position = new Vector3(-x, -0.6f, 0f);
        // Reference the resources
        ResourceController[] controllers = GetComponents<ResourceController>();
        unitResourceController = controllers[0];
        spellResourceController = controllers[1];
        // Create Pools for Units and Spells
        int cUnits = 0;
        int cSpells = 0;
        CountDeck(out cUnits, out cSpells);
        units = new Pool<UnitController>[cUnits];
        for (int i = 0; i < units.Length; i++)
        {
            int t = i;
            units[i] = new Pool<UnitController>(() => {
                return UnitFactory(t);
            });
            InterfaceManager.Instance.BuildCardGUI(unitLineup[i], () => { Spawn(t); });
        }
        spells = new Pool<SpellController>[cSpells];
        for (int i = 0; i < spells.Length; i++)
        {
            int t = i;
            spells[i] = new Pool<SpellController>(() => {
                return SpellFactory(t);
            });
            InterfaceManager.Instance.BuildCardGUI(spellLineup[i], () => { Cast(t); });
        }
    }

    private void CountDeck(out int units, out int spells)
    {
        units = deck.Where(x => x is Unit).Count();
        spells = deck.Where(x => x is Spell).Count();
        spellLineup = new Spell[spells];
        unitLineup = new Unit[units];
        int len = deck.Length;
        for (int i = 0, iSpell = 0, iUnit = 0; i < len; i++)
        {
            Card c = deck[i];
            if (c is Spell)
            {
                Spell spell = c as Spell;
                spellLineup[iSpell] = spell;
                iSpell++;
            }
            else if (c is Unit)
            {
                Unit unit = c as Unit;
                unitLineup[iUnit] = unit;
                iUnit++;
            }
        }
    }

    private SpellController SpellFactory(int index)
    {
        Spell u = spellLineup[index];
        GameObject go = Instantiate(u.prefab);
        go.name = name + "|" + u.name;
        go.transform.SetParent(transform);
        SpellController spellController = go.Ext_AddOrGetCompononent<SpellController>();
        spellController.spell = u;
        spellController.building = allyBase;
        return spellController;
    }
    private UnitController UnitFactory(int index)
    {
        Unit u = unitLineup[index];
        GameObject go = Instantiate(u.prefab);
        // GameObject go = new GameObject(name + "|"+ u.name);
        go.name = name + "|" + u.name;
        go.transform.SetParent(transform);
        go.layer = gameObject.layer;
        UnitController uc = go.Ext_AddOrGetCompononent<UnitController>();
        uc.unit = u;
        uc.enemyMask = enemyMask;
        uc.target = enemyBase.transform;
        return uc;
    }
    public virtual void Spawn(int index)
    {
        UnitController controller = null;
        if (unitResourceController.PayCost(unitLineup[index]))
        {
            controller = units[index].Acquire();
            controller.Ready(this, index);
            if (debug) Debug.Log("Acquiring Unit " + controller.unit.name + " with index " + index);
        }
    }
    public virtual void Cast(int index)
    {
        SpellController controller = null;
        if (spellResourceController.PayCost(spellLineup[index]))
        {
            controller = spells[index].Acquire();
            controller.Ready(this, index);
            if (debug) Debug.Log("Acquiring Spell " + controller.spell.name + " with index " + index);
        }
    }
    public void Release(UnitController unit, int index)
    {
        if (debug) Debug.Log("Releasing Unit " + unit.name + " with index " + index);
        units[index].Release(unit);
    }
    public void Release(SpellController spell, int index)
    {
        if (debug) Debug.Log("Releasing Spell " + spell.name + " with index " + index);
        spells[index].Release(spell);
    }
}