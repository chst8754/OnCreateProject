﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpellTarget : MonoBehaviour {

    // Properties
    // Healthpoints, AttackDamage, AttackRange, AttackSpeed, Movementspeed, Spawn, Death, Attack
    public HealthbarController health;
    public UnitController unit;
    public BuildingController building;
    // private List<BuffSpell> appliedBuffs;
    void Start()
    {
        health = GetComponent<HealthbarController>();
        unit = GetComponent<UnitController>();
        building = GetComponent<BuildingController>();
        // appliedBuffs = new List<BuffSpell>();
    }
    public void ApplySpell(Spell spell)
    {
        spell.ApplySpell(this);
    }
    /*public void ApplyBuff(BuffSpell buff)
    {
        // Add buff to list
        // appliedBuffs.Add(buff);
        Action initial = null;
        VoidFloatParam ot = null;
        int len = buff.affectedTypes.Length;
        for (int i=0; i < len; i++)
        {
            UnitPropertyType prop = buff.affectedTypes[i];
            switch (prop)
            {
                case UnitPropertyType.AttackDamage:
                    initial = () => unit.attackPoints = buff.Buff(unit.attackPoints);
                    break;
                case UnitPropertyType.AttackRange:
                    initial = () => unit.attackRange = buff.Buff(unit.attackRange);
                    break;
                case UnitPropertyType.AttackSpeed:
                    initial = () => unit.attackSpeed = buff.Buff(unit.attackSpeed);
                    break;
                case UnitPropertyType.Movementspeed:
                    break;
                case UnitPropertyType.Healthpoints:
                    initial = () => health.healthPoints = buff.Buff(health.healthPoints);
                    break;
                case UnitPropertyType.Death:
                    break;
                case UnitPropertyType.Attack:
                    break;
                default:
                    Debug.Log("Something went very wrong with " + name);
                    break;
            }
            ot = (f) =>
            {
                Debug.Log(buff.name + " is active on " + name);
            };
        }
        StartCoroutine(OverTime(buff.duration, initial, ot));
    }*/
    public delegate void VoidFloatParam(float f);
    public IEnumerator OverTime(float duration, Action initialEffect, VoidFloatParam otEffect = null)
    {
        float timePassed = 0f;
        if (initialEffect != null)
        {
            initialEffect();
        }
        bool isNotNull = otEffect != null;
        if (isNotNull)
        {
            while (timePassed < duration && health.IsAlive)
            {
                timePassed += Time.deltaTime;
                otEffect(timePassed);
                yield return null;
            }
        }
        else
        {
            while (timePassed < duration && health.IsAlive)
            {
                timePassed += Time.deltaTime;
                yield return null;
            }
        }
    }
}
