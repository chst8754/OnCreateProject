﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
[CreateAssetMenu(menuName = "OnCreate/Campaign/New Campaign")]
public class Campaign : ScriptableObject
{
    public new string name;
    [TextArea]
    public string description;
    public Sprite sprite;
    public Level[] levels;
    public Card[] unlocks;
    public bool unlocked;
    public bool beaten;
}
