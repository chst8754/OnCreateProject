﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "OnCreate/Campaign/New Level")]
public class Level : ScriptableObject {
    [Header("Level Properties")]
    public new string name;
    [TextArea]
    public string description;
    public Boss boss;
    public Card[] unlocks;
    public bool unlocked;
    public bool beaten;
}