﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "OnCreate/Boss/New Boss")]
public class Boss : ScriptableObject
{
    public new string name;
    [TextArea]
    public string description;
    public Card[] cards;
    public int baseHealthPoints;
    public Resource foodResource;
    public Resource spellResource;
}
