﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISaveable<T>
{
    T Save();
    void Load();
}
