﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Extensions
{
    /// <summary>
    /// Draws a cross out of at the specified location with the specified scale
    /// </summary>
    /// <param name="origin"></param>
    /// <param name="scale"></param>
    /// <param name="c"></param>
    public static void Ext_DrawCross(Vector3 origin, float scale, Color c)
    {
        Debug.DrawRay(origin + ((Vector3.up + Vector3.left) / 2f) * scale, (Vector3.down + Vector3.right) * scale, c);
        Debug.DrawRay(origin + ((Vector3.up + Vector3.right) / 2f) * scale, (Vector3.down + Vector3.left) * scale, c);
    }
    /// <summary>
    /// Returns the inverted color
    /// </summary>
    /// <param name="color"></param>
    /// <returns></returns>
    public static Color Ext_Invert(this Color color)
    {
        return new Color(1f - color.r, 1f - color.g, 1f - color.b);
    }
    public static T Ext_AddOrGetCompononent<T>(this GameObject obj) where T : Component
    {
        T com = obj.GetComponent<T>();
        if (com == null)
        {
            com = obj.AddComponent<T>();
        }
        return com;
    }
}
