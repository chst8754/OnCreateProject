﻿using System;
using System.Linq;
using System.Reflection;
using UnityEditor;

public class PropertyDrawerUtility
{
    /// <summary>
    /// http://sketchyventures.com/2015/08/07/unity-tip-getting-the-actual-object-from-a-custom-property-drawer/
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <param name="fieldInfo"></param>
    /// <param name="property"></param>
    /// <returns></returns>
    public static T GetActualObjectForSerializedProperty<T>(FieldInfo fieldInfo, SerializedProperty property) where T : class
    {
        var obj = fieldInfo.GetValue(property.serializedObject.targetObject);
        if (obj == null) { return null; }

        T actualObject = null;
        if (obj.GetType().IsArray)
        {
            var index = Convert.ToInt32(new string(property.propertyPath.Where(c => char.IsDigit(c)).ToArray()));
            actualObject = ((T[])obj)[index];
        }
        else
        {
            actualObject = obj as T;
        }
        return actualObject;
    }
}