﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(Pool<IReuseable>))]
public class PoolEditor : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        base.OnGUI(position, property, label);
        Pool<IReuseable> pool = PropertyDrawerUtility.GetActualObjectForSerializedProperty<Pool<IReuseable>>(fieldInfo, property);
    }
}
