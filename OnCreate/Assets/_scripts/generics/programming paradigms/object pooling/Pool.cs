﻿using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Generic container class for Object Pooling.
/// <para>Implement the interface <see cref="IReuseable"/> into any class you want to object pool</para>
/// </summary>
/// <typeparam name="T"></typeparam>
public class Pool<T> where T : IReuseable
{
    /// <summary>
    /// List of objects ready for use
    /// </summary>
    private List<T> reusables;
    /// <summary>
    /// The default factory method for object creation
    /// </summary>
    /// <example><code>
    /// () => new T();
    /// // or 
    /// () => {
    ///     T foo = new T();
    ///     foo.Bar();
    ///     return foo;
    /// }
    /// </code></example>
    private Func<T> defaultFactory;
    public int Count
    {
        get
        {
            return (reusables == null) ? 0 : reusables.Count;
        }
    }
    public Pool(Func<T> defaultFactory, List<T> reusables = null)
    {
        this.Setup(defaultFactory, reusables);
    }

    /// <summary>
    /// Use this for Initialization, because Constructors sometimes work funny inside Unity
    /// <para>Func&lt;T&gt; defaultFactory example: <c>() =&gt; new T();</c></para>
    /// </summary>
    /// <param name="defaultFactory"><see cref="defaultFactory"/></param>
    /// <param name="reusables"><see cref="reusables"/></param>
    public void Setup(Func<T> defaultFactory, List<T> reusables = null)
    {
        this.defaultFactory = defaultFactory;
        this.reusables = (reusables == null) ? new List<T>() : reusables;
    }
    /// <summary>
    /// Release T item for further usage
    /// </summary>
    /// <param name="item">Item to release</param>
    public void Release(T item)
    {
        if (item != null)
        {
            reusables.Add(item);
        }
        else
        {
            throw new ArgumentNullException("Pool::Release(Null)");
        }
    }
    /// <summary>
    /// Returns an item from the pool or a default one if there are none available
    /// </summary>
    /// <returns></returns>
    public T Acquire()
    {
        T item;
        if (reusables.Count > 0)
        {
            item = reusables.First();
            reusables.Remove(item);
        }
        else
        {
            item = defaultFactory();
            // Debug.LogWarning("Pool<" + ((item == null) ? "NULL" : item.GetType().ToString()) + "> had to instantiate a default item!");
        }
        return item;
    }
}