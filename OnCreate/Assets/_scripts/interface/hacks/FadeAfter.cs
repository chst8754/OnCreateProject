﻿    using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class FadeAfter : MonoBehaviour {

    public bool skip;
    public AnimationCurve curve;
    public GameObject[] disableOnStart;
    public GameObject[] activateAfterEnd;

    void OnEnable()
    {
        for (int i = 0; i < activateAfterEnd.Length; i++)
        {
            activateAfterEnd[i].SetActive(false);
        }
    }

    public void Fade(float seconds)
    {
        for (int i = 0; i < disableOnStart.Length; i++)
        {
            disableOnStart[i].SetActive(false);
        }
        StartCoroutine(RadialFade(seconds));
    }
    private IEnumerator Fading(float seconds)
    {
        Image image = GetComponent<Image>();
        float timePassed = 0f;
        Color result = new Color(image.color.r, image.color.g, image.color.b, 0f);
        while (timePassed < seconds && !skip)
        {
            timePassed += Time.deltaTime;
            image.color = Color.Lerp(image.color, result, curve.Evaluate(timePassed));
            yield return null;
        }
        gameObject.SetActive(false);
        for (int i = 0; i < activateAfterEnd.Length; i++)
        {
            activateAfterEnd[i].SetActive(true);
        }
    }
    private IEnumerator RadialFade(float seconds)
    {
        Image image = GetComponent<Image>();
        float timePassed = 0f;
        while (timePassed < seconds && !skip)
        {
            timePassed += Time.deltaTime;
            image.fillAmount = 1f - timePassed / seconds;
            yield return null;
        }
        gameObject.SetActive(false);
        for (int i = 0; i < activateAfterEnd.Length; i++)
        {
            activateAfterEnd[i].SetActive(true);
        }
    }
}