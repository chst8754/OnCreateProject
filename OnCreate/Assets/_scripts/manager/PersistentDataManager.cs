﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersistentDataManager : Singleton<PersistentDataManager> {

    public Card[] player_deck;
    public Level next_level;
}
