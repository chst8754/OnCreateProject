﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class InterfaceManager : Singleton<InterfaceManager> {

    public bool isPaused;
    public Vector2 ScreenSize
    {
        get
        {
            return new Vector2(Screen.width, Screen.height);
        }
    }
    public GUIComposition[] compositions;
    public Canvas canvas;
    public CardInfo cardInfo;
    public CardSelection cardSelection;
    public GameObject cardParent;
    public GameObject cardPrefab;
    public int maximumCards;
    private int internalCounter;
    public Image resourceBar;
    public Image manaBar;
    public Sprite missingSprite;

    void Start () {
        internalCounter = 0;
        isPaused = false;
        Time.timeScale = 1f;
        for (int i = 0; i < compositions.Length; i++) {
            compositions[i].Inflate(this, ScreenSize);
        }
	}
    private Text unitCurrent;
    private Text manaCurrent;
    private int maxUnitRes;
    private int maxManaRes;

    private Card lastSelectedCard;
    private GameObject lastSelectedCardGO;
    public void CardSelected(Card selectedCard, GameObject selectedCardGO)
    {
        if (this.lastSelectedCard == null)
        {
            lastSelectedCard = selectedCard;
            lastSelectedCardGO = selectedCardGO;
        }
        else
        {
            if (lastSelectedCard == selectedCard)
            {
                // Same Card selected twice => Unselect
                lastSelectedCard = null;
                lastSelectedCardGO = null;
            }
            else
            {
                // Selected other card
                RectTransform currentlySelected = selectedCardGO.GetComponent<RectTransform>();
                RectTransform recentlySelected = lastSelectedCardGO.GetComponent<RectTransform>();

                Vector3 posA = currentlySelected.position;
                Vector3 posB = recentlySelected.position;
                // Switch positions
                currentlySelected.GetComponent<RectTransform>().position = posB;
                recentlySelected.GetComponent<RectTransform>().position = posA;

                // Change parents
                cardSelection.PickCard(selectedCard, selectedCardGO, lastSelectedCard, lastSelectedCardGO);

                // and Unselect
                lastSelectedCard = null;
                lastSelectedCardGO = null;
            }
        }
    }

    public void BuildResourceGUI(Resource res)
    {
        // set low and high
        Text low = resourceBar.transform.GetChild(0).GetComponent<Text>();
        low.text = res.baseMinimum.ToString();
        Text high = resourceBar.transform.GetChild(1).GetComponent<Text>();
        high.text = res.baseMaximum.ToString();
        unitCurrent = resourceBar.transform.GetChild(2).GetComponent<Text>();
        unitCurrent.text = res.baseMinimum.ToString();
        maxUnitRes = res.baseMaximum;
        resourceBar.GetComponent<RectTransform>().sizeDelta = new Vector2(0f, (ScreenSize.y / 100f) * 5f);
    }
    public void UpdateResourceGUI(int amount, int newMaxRes = -1)
    {
        unitCurrent.text = amount.ToString();
        resourceBar.fillAmount = amount * (1f / ((newMaxRes < 0) ? maxUnitRes : newMaxRes));
    }
    public void BuildManaGUI(Resource res)
    {
        // set low and high
        Text low = manaBar.transform.GetChild(0).GetComponent<Text>();
        low.text = res.baseMinimum.ToString();
        Text high = manaBar.transform.GetChild(1).GetComponent<Text>();
        high.text = res.baseMaximum.ToString();
        manaCurrent = manaBar.transform.GetChild(2).GetComponent<Text>();
        manaCurrent.text = res.baseMinimum.ToString();
        maxManaRes = res.baseMaximum;
        manaBar.GetComponent<RectTransform>().sizeDelta = new Vector2(0f, (ScreenSize.y / 100f) * 5f);
        manaBar.GetComponent<RectTransform>().position += new Vector3(0f, (ScreenSize.y / 100f) * 5f, 0f);
    }
    public void UpdateManaGUI(int amount, int newMaxRes = -1)
    {
        manaCurrent.text = amount.ToString();
        manaBar.fillAmount = amount * (1f / ((newMaxRes < 0) ? maxManaRes : newMaxRes));
    }
    /// <summary>
    /// For Interface purposes only
    /// </summary>
    /// <param name="card"></param>
    /// <param name="preferredSize"></param>
    /// <param name="preferredPosition"></param>
    /// <param name="action"></param>
    /// <returns></returns>
    public GameObject CreateCardGUI(Card card, Vector2 preferredSize, Vector2 preferredPosition, Action<Card> action)
    {
        // setup a gameobject
        GameObject go = Instantiate(cardPrefab);

        go.transform.GetChild(0).gameObject.SetActive(false);
        go.transform.GetChild(1).gameObject.SetActive(false);
        Image image = go.transform.GetChild(2).GetComponent<Image>();
        Text cost = go.transform.GetChild(3).GetComponent<Text>();
        RectTransform rt = go.GetComponent<RectTransform>();
        Button button = image.gameObject.GetComponent<Button>();

        go.name = card.name;
        image.sprite = card.cardSprite;
        // @TODO : Sprite Änderung machen
        
        cost.text = card.baseCost.ToString();
        image.name = card.name;

        // button.onClick.AddListener(() => { CardSelected(card, go); });
        button.onClick.AddListener(() => { action(card); cardInfo.UpdatedDisplay(card); });
        // scale cards according to screen dimensions ( 88.9 x 63.50 | 1.4 x 1 )
        rt.sizeDelta = preferredSize;
        // position sprite
        rt.pivot = Vector2.zero;
        rt.position = preferredPosition;
        return go;
    }
    public void MaybeShowCardInfo(Card card)
    {
        Debug.Log("Maybe Show card");
        StartCoroutine(ListenForInput(card));
    }
    private IEnumerator ListenForInput(Card card)
    {
        float timePassed = 0f;
        bool sufficient = true;
        while (timePassed < 0.5f && sufficient)
        {
            timePassed += Time.deltaTime;
            if (Application.isEditor)
            {
                sufficient = sufficient && Input.GetMouseButton(0);
            }
            else
            {
                sufficient = sufficient && Input.touchCount > 1;
            }
            yield return null;
        }
        if (sufficient)
        {
            cardInfo.UpdatedDisplay(card);
        }
        else
        {
            Debug.Log("Maybe -> no");
        }
    }
    public void BuildCardGUI(Card card, Action action)
    {
        /*if (internalCounter >= cardSlots.Length)
        {
            Debug.LogWarning("Too many cards " + internalCounter);
            return;
        }
        */
        // setup a gameobject
        GameObject go = Instantiate(cardPrefab);
        // cardSlots[internalCounter] = card;
        internalCounter++;
        go.transform.SetParent(cardParent.transform);
        go.name = card.name;
        // Card prefab, all children have Image components
        /* Card
         * |-> Border       (Image)
         * |-> Background   (Image)
         * |-> Image        (Image, Button)
         */
        // Image border = go.transform.GetChild(0).GetComponent<Image>();
        // Image background = go.transform.GetChild(1).GetComponent<Image>();
        go.transform.GetChild(0).gameObject.SetActive(false);
        go.transform.GetChild(1).gameObject.SetActive(false);
        Image image = go.transform.GetChild(2).GetComponent<Image>();
        Text cost = go.transform.GetChild(3).GetComponent<Text>();
        if (card.cardSprite == null)
        {
            // border.sprite = card.defaultBorder;
            // background.color = card.defaultBackgroundColor;
            image.sprite = card.defaultSprite;
        }
        else
        {
            image.sprite = card.cardSprite;
        }
        cost.text = card.baseCost.ToString();
        
        RectTransform rt = go.GetComponent<RectTransform>();
        // scale cards according to screen dimensions ( 88.9 x 63.50 | 1.4 x 1 )
        Vector2 newSize = new Vector2(((ScreenSize.x / 100f) * 10f), 1.4f * ((ScreenSize.x / 100f) * 10f));
        rt.sizeDelta = newSize;
        // position sprite
        rt.pivot = Vector2.zero;
        rt.position = new Vector2((ScreenSize.x / maximumCards) * (internalCounter - 1), (ScreenSize.y / 100f) * 10f);

        // Action
        Button button = image.gameObject.GetComponent<Button>();
        button.onClick.AddListener(() => { action(); });
    }
}