﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : Singleton<LevelManager> {
    /// <summary>
    /// Changes the current scene to the scene with the specified index sceneId
    /// </summary>
    /// <param name="sceneId"></param>
    public void ChangeScene(int sceneId)
    {
        SceneManager.LoadScene(sceneId);
    }
    private void OnEnable()
    {
        DontDestroyOnLoad(gameObject);

        SceneManager.sceneLoaded += OnLevelFinishedLoading;
    }
    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnLevelFinishedLoading;
    }
    public void OnLevelFinishedLoading(Scene scene, LoadSceneMode mode)
    {
        if (scene.buildIndex == 0)
        {
            // Main Menu: Unlock all the cards and levels

            /* 
            Selection Screen
            - Show current progress   
            - Show all levels
            - Options
            */
        }else if (scene.buildIndex == 1)
        {
            // Level: Setup the level
        }
    }
}
