﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    public GameObject campaignParent;
    public Campaign[] campaigns;
    public Sprite locked;
    public Sprite unlocked;
    public GameObject levelPrefab;
    public GameObject campaignPrefab;
    private RectTransform lprt;
    private Vector2 dimension;
    private int campaignCounter = 2;
    public CardSelection cardSelection;
    // Interface Update 
    private GameObject playerCard, info, challenge, brawl, campaign, cards, talents;

    void Start()
    {
        // Acquire references
        playerCard = transform.FindChild("PlayerCard").gameObject;
        info = transform.FindChild("Info").gameObject;
        challenge = transform.FindChild("Challenge").gameObject;
        brawl = transform.FindChild("Brawl").gameObject;
        campaign = transform.FindChild("Campaign").gameObject;
        cards = transform.FindChild("Cards").gameObject;
        talents = transform.FindChild("Talents").gameObject;
        // Setup functionality
        Button campaignButton = campaign.GetComponent<Button>();
        Button cardsButton = cards.GetComponent<Button>();
        campaignButton.onClick.AddListener(() =>
        {
            PersistentDataManager.Instance.next_level = campaigns[0].levels.Where(x => x.unlocked).LastOrDefault();
            Load(PersistentDataManager.Instance.next_level);
        });
        cardsButton.onClick.AddListener(() =>
        {
            gameObject.SetActive(false);
            cardSelection.gameObject.SetActive(true);
        });
        // Scale with the resolution
        // Note to self: Anchors are what I want most of the time...
    }

    void Start2 () {
        lprt = campaignParent.GetComponent<RectTransform>();
        dimension = InterfaceManager.Instance.ScreenSize;
        lprt.sizeDelta = new Vector2(0f, dimension.y * 0.15f);
        int len = campaigns.Length;
        Vector2 campaignDim = new Vector2(dimension.x * 0.35f, dimension.y * 0.60f);
        Vector2 levelDim = new Vector2(campaignDim.x * 0.3f, campaignDim.x * 0.3f);
        Vector2 pos = new Vector2((campaignDim.x / 2f), 0f);
        Vector2 gap = new Vector2(dimension.x * 0.05f, 0f);

        for (int currentCampaign = 0; currentCampaign < len; currentCampaign++)
        {
            Campaign campaign = campaigns[currentCampaign];
            Level[] levels = campaign.levels;
            int t_len = levels.Length;

            GameObject campaignGO = Instantiate(campaignPrefab);
            campaignGO.name = campaign.name;
            campaignGO.transform.SetParent(campaignParent.transform);

            GameObject levelParent = new GameObject("Levels");
            levelParent.transform.SetParent(campaignGO.transform);

            Image image = campaignGO.Ext_AddOrGetCompononent<Image>();
            image.sprite = campaign.sprite;

            Button button = campaignGO.Ext_AddOrGetCompononent<Button>();
            button.onClick.AddListener(() =>
            {
                PersistentDataManager.Instance.next_level = levels.Where(x => x.unlocked).LastOrDefault();
                gameObject.SetActive(false);
                cardSelection.gameObject.SetActive(true);
            });
            button.interactable = campaign.unlocked;

            RectTransform rt = campaignGO.GetComponent<RectTransform>();
            rt.sizeDelta = campaignDim;
            rt.localPosition = Vector2.zero;

            for (int currentLevel = 0; currentLevel < t_len; currentLevel++)
            {
                Level level = levels[currentLevel];

                GameObject go = Instantiate(levelPrefab);
                go.name = level.name;
                go.transform.SetParent(levelParent.transform);

                image = go.Ext_AddOrGetCompononent<Image>();
                image.sprite = (level.unlocked) ? unlocked : locked;

                button = go.Ext_AddOrGetCompononent<Button>();
                button.onClick.AddListener(() => {
                    PersistentDataManager.Instance.next_level = level;
                    gameObject.SetActive(false);
                    cardSelection.gameObject.SetActive(true);
                });
                button.interactable = level.unlocked;

                Text text = go.transform.GetChild(0).GetComponent<Text>();
                text.text = (currentLevel + 1).ToString();

                rt = go.GetComponent<RectTransform>();
                rt.sizeDelta = new Vector2(levelDim.x - gap.x / 2f, levelDim.y - gap.x / 2f);
                rt.localPosition = -pos + new Vector2(((campaignDim.x/2f) / len) * currentLevel, -campaignDim.y * 0.65f);
            }
            if (currentCampaign > 0)
            {
                campaignGO.SetActive(false);
            }
        }

        GameObject leftGO = campaignParent.transform.GetChild(0).gameObject;
        GameObject rightGO = campaignParent.transform.GetChild(1).gameObject;
        if (campaignParent.transform.childCount > 3)
        {

            RectTransform left = leftGO.GetComponent<RectTransform>();
            RectTransform right = rightGO.GetComponent<RectTransform>();
            Vector2 buttonDim = new Vector2(dimension.y * 0.1f, dimension.x * 0.1f);
            left.sizeDelta = buttonDim;
            right.sizeDelta = buttonDim;
            left.localPosition = new Vector2(-campaignDim.x / 2f, 0f) - gap;
            right.localPosition = new Vector2(campaignDim.x / 2f, 0f) + gap;

            Button lB = leftGO.Ext_AddOrGetCompononent<Button>();
            Button rB = rightGO.Ext_AddOrGetCompononent<Button>();
            lB.onClick.AddListener(() =>
            {
                GoLeft();
            });
            rB.onClick.AddListener(() =>
            {
                GoRight();
            });
            leftGO.SetActive(true);
            rightGO.SetActive(true);
        }
        else
        {
            leftGO.SetActive(false);
            rightGO.SetActive(false);
        }
        maxChilds = campaignParent.transform.childCount-1;
    }
    private int maxChilds;
    
    void GoLeft()
    {
        campaignParent.transform.GetChild(campaignCounter).gameObject.SetActive(false);
        if ((campaignCounter-1) < 2)
        {
            campaignCounter = maxChilds;
        }
        else
        {
            campaignCounter--;
        }
        campaignParent.transform.GetChild(campaignCounter).gameObject.SetActive(true);
    }

    void GoRight()
    {
        campaignParent.transform.GetChild(campaignCounter).gameObject.SetActive(false);
        if ((campaignCounter + 1) > maxChilds)
        {
            campaignCounter = 2;
        }
        else
        {
            campaignCounter++;
        }
        campaignParent.transform.GetChild(campaignCounter).gameObject.SetActive(true);
    }

    void Load(Level level)
    {
        if (PersistentDataManager.Instance.player_deck.Length == 8)
        {
            LevelManager.Instance.ChangeScene(1);
        }
    }
}
