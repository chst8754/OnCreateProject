﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum UnitPropertyType
{
    Healthpoints, AttackDamage, AttackRange, AttackSpeed, Movementspeed, Spawn, Death, Attack
}
